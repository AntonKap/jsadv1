"use strict"
class Employee{
    constructor(name = "", age = 0, salary = 0){
this._name = name;
this._age = age;
this._salary = salary;
    }
get name(){
    return this._name ;
}
set name(newName){
    if (newName === ""){
        console.warn("Name not  entered"); 
    }
    this._name = newName;
}
get age(){
    return this._age
}
set age(newAge){
    this._age = newAge
}

get salary(){
    return this._salary
}
set salary(newSalary){
    this._salary = newSalary 
}

show(){
    console.log(`My name is ${this._name}. I am ${this.age}. I have salary ${this.salary} dollars `)
}

}

class Programmer extends Employee{
    constructor(name, age, salary, leng){
        super(name, age, salary)
        this.leng = leng;
    }
    get salary(){
        return this._salary* 3
    }
    choiseLang(){
        console.log(`My name is ${this._name}. I am ${this.age}.I have salary ${this.salary} dollars . My lang is ${this.leng}`);
    }
}

let employee = new Employee("Wer", 12, 1234);
employee.show();
// employee.name = ''
// console.log(employee)

let programmer = new Programmer("Ben", 34, 9049, "eng");
programmer.choiseLang();
let programmer2 = new Programmer("Eveb", 35, 1000, "en");
programmer2.choiseLang();

